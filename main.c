#include <stdio.h>
#include <string.h>
#include <stdlib.h>

double** inicializaMatriz(int, int);
void defineFuncaoObjetivo(int*, int, int*, int, double*);
void defineLimites(int quantProdutos, int quantCompostos, double* compostoPorProduto, int* limiteComposto);

int main() {
    // recebe todas as entradas
    int numeroProdutos, numeroCompostos;

    scanf("%d", &numeroProdutos);
    scanf("%d", &numeroCompostos);

    int valorProdutoArray[numeroProdutos];
    for(int index = 0; index < numeroProdutos; index++) {
        scanf("%d", &valorProdutoArray[index]);
    }

    int custoCompostoArray[numeroCompostos];
    int limiteCompostoArray[numeroCompostos];

    for(int index = 0; index < numeroCompostos; index++) {
        scanf("%d", &custoCompostoArray[index]);
        scanf("%d", &limiteCompostoArray[index]);
    }

    double* compostoPorProduto = malloc(numeroProdutos * numeroCompostos * sizeof (double));
    for(int i = 0; i < numeroProdutos; i++) {
        for(int j = 0; j < numeroCompostos; j++) {
            scanf("%lf", &compostoPorProduto[(i*numeroCompostos) + j]);
        }
    }

    // seta o problema
    defineFuncaoObjetivo(valorProdutoArray, numeroProdutos, custoCompostoArray, numeroCompostos, compostoPorProduto);

    defineLimites(numeroProdutos, numeroCompostos, compostoPorProduto, limiteCompostoArray);

    return 0;
}

void defineFuncaoObjetivo(int* valorProdutos, int quantProdutos, int* custoComposto, int quantCompostos, double* compostoPorProduto) {
    printf("max : ");
    for(int i = 0; i < quantProdutos; i++) {
        float custo = 0;
        for(int j = 0; j < quantCompostos; j++) {
            custo += custoComposto[j] * compostoPorProduto[(i*quantCompostos) + j];
        }
        printf("%.2fx%d", valorProdutos[i] - custo, i+1);

        // se chegar no final, finaliza a string com ;
        if(i == quantProdutos - 1) printf(";");
        else printf(" + ");
    }

    printf("\n\n");
}

void defineLimites(int quantProdutos, int quantCompostos, double* compostoPorProduto, int* limiteComposto) {
    // escreve no formato <valor usado do composto para fabricar o produto>*xi <= limiteComposto;
    for(int j = 0; j < quantCompostos; j++) {
        for(int i = 0; i < quantProdutos; i++) {
            if(i > 0) printf(" + ");
            printf("%.2lfx", compostoPorProduto[(i*quantCompostos) + j]);
            printf("%d", i+1);
        }
        printf(" <= %d;\n", limiteComposto[j]);
    }

    printf("\n");
}
